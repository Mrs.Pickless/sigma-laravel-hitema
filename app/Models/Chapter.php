<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    use HasFactory;

    protected $table ="chapters";
    protected $fillable = ['title',"content","formations"];

    public function getFormation(){
        return $this->belongsTo(Formation::class,'id',"formations");
    }
}
