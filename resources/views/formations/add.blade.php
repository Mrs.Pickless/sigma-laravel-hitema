@extends("layouts.header")
@section('content')
<div class="container">
<h1>ajouter une formation </h1>
@if($errors->any())
<ul class="alert alert-danger">
@foreach($errors->all() as $error)
<li>{{$error}}</li>
@endforeach
</ul>
@endif

<form method="POST" action="" enctype="multipart/form-data">
    @csrf
    @method('POST')
    <div class="form-group">
        <label>Titre</label>
        <input type="text" class="form-control" placeholder="" name="title" >
    </div>

    <div class="form-group">
        <label>Description</label>
        <textarea type="text" class="form-control" rows="5" name="description" ></textarea>
    </div>

    <div class="form-group">
        <label>prix</label>
        <input type="number" class="form-control" placeholder="" name="prix" >
    </div>

    <div class="form-group">
        <label>image ( entrer une url )</label>
        <input type="text" class="form-control" placeholder="" name="picture" >
    </div>

    <div>
        <label>Catégorie de la formation</label>
        @foreach($categories as $category)
            <div class="form-check form-check-inline">
                <input type="checkbox" class="form-check-input" id="check-{{$category->id}}"
                       name="checkboxCategories[{{$category->id}}]"
                       value="{{$category->id}}">
                <label for="check-{{$category->id}}" class="form-check-label">{{$category->title}}</label>
            </div>
        @endforeach
    </div>

    <button type="submit" class="btn btn-primary">Ajouter</button>
</form>


</div>
@endsection
