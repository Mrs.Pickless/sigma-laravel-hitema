@extends("layouts.header")
@section('content')
<div class="container">
    <h1>Listes des categories</h1>
<ul>
    @foreach ($categories as $category)
        <li>{{$category->title}}</li>
    @endforeach
</ul>
<a href="{{route("categoryAdd")}}" class="btn btn-primary">Ajouter une catégorie</a>
</div>
@endsection