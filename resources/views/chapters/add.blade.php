<h1>Ajouter un chapitre </h1>
<form method="POST" action="{{route('chapitresStore',$formation->id)}}" enctype="multipart/form-data">
    @csrf
    @method('POST')
    <div class="form-group">
        <label>Titre</label>
        <input type="text" class="form-control" placeholder="" name="title" >
    </div>

    <div class="form-group">
        <label>contenue</label>
        <textarea type="text" class="form-control" rows="5" name="description" ></textarea>
    </div>

    <button type="submit" class="btn btn-primary">Ajouter</button>
</form>
